"use client";
import { Field, Form, Formik } from "formik";
import { signIn } from "next-auth/react";
import Link from "next/link";
import { useRouter } from "next/navigation";
import toast from "react-hot-toast";
import { FcGoogle } from "react-icons/fc";
import * as Yup from "yup";
import "../styles/register.css";

import axios from "axios";

const SigninSchema = Yup.object().shape({
  email: Yup.string().email("Invalid email").required("Required"),
  password: Yup.string().required("Required"),
});

const login = () => {
  const router = useRouter();
  const handleLogin = async (values) => {
    try {
      await axios.post("/api/auth/signin", values).then((response) => {
        console.log(response);
        if (response.data.status === 404) {
          toast.error("User not found");
          return;
        } else if (response.data.status === 401) {
          toast.error("Invalid email or password");
          return;
        }
        localStorage.setItem("accessToken", JSON.stringify(response.data));
        if (response.data.user.isAdmin) {
          localStorage.setItem("isAdmin", "true");
          router.push("/admin");
          return;
        }
        toast.success("Signed in successfully");
        // Redirect user to dashboard or desired page
        router.push("/dashboard");
      });
    } catch (err) {
      console.log(err);
    }
  };
  return (
    <div className="authForm">
      <h1>Signin</h1>
      <Formik
        initialValues={{
          email: "",
          password: "",
        }}
        validationSchema={SigninSchema}
        onSubmit={(values) => {
          handleLogin(values);
          // same shape as initial values
          console.log(values);
        }}
      >
        {({ errors, touched }) => (
          <Form autoComplete="off" className="formField">
            <Field name="email" type="email" placeholder="email" />
            {errors.email && touched.email ? <div>{errors.email}</div> : null}
            <Field name="password" type="password" placeholder="password" />
            {errors.password && touched.password ? (
              <div>{errors.password}</div>
            ) : null}
            <br />
            Don't have an account? <Link href="/register">Signup</Link>
            <br />
            <button type="submit">Submit</button>
            <br />
          </Form>
        )}
      </Formik>
      {/* <div className="google-signin-button">
        or
        <br />
        <button
          type="button"
          onClick={() => signIn("google")}
          className="google-signin-button-btn"
        >
          Sign in with
          <FcGoogle />
        </button>
      </div> */}
    </div>
  );
};

export default login;
