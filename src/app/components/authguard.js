import { useRouter } from "next/navigation";
import { useEffect } from "react";

const AuthGuard = ({ children }) => {
  const router = useRouter();

  useEffect(() => {
    // Check if user is authenticated (for demo, we use local storage)
    const isAuthenticated = localStorage.getItem("accessToken");
    const isAdmin = localStorage.getItem("isAdmin");

    // If user is not authenticated, redirect to login page
    if (!isAuthenticated || isAdmin) {
      router.push("/signin");
    }
  }, []);
  return children;
};

export default AuthGuard;
