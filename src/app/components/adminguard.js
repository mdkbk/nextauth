import { useRouter } from "next/navigation";
import { useEffect } from "react";

const AdminGuard = ({ children }) => {
  const router = useRouter();

  useEffect(() => {
    const isAdmin = localStorage.getItem("isAdmin");
    if (!isAdmin) {
      router.push("/signin");
    }
  }, []);

  return children;
};

export default AdminGuard;
