import { useRouter } from "next/navigation";
import { signIn } from "next-auth/react";

export default function SignInWithGoogleButton() {
  const router = useRouter();

  const handleSignInWithGoogle = async () => {
    const result = await signIn("google", {
      callbackUrl: "http://localhost:3000/dashboard",
    });

    if (result.error) {
      // Handle sign-in error
      console.error("Failed to sign in with Google:", result.error);
    } else {
      // Redirect to the dashboard page
      router.push("/dashboard");
    }
  };

  return <button onClick={handleSignInWithGoogle}>Sign in with Google</button>;
}
