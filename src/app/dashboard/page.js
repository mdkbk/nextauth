"use client";

import AuthGuard from "../components/authguard";
import { signOut } from "next-auth/react";
import { useRouter } from "next/navigation";
import toast from "react-hot-toast";

const Dashboard = () => {
  const router = useRouter();

  const handleSignOut = async () => {
    try {
      // await signOut();
      localStorage.removeItem("accessToken");
      toast.success("Successfully logged out!");
      setTimeout(() => {
        router.replace("/signin");
      }, 1200);
    } catch (error) {
      console.error("Error signing out:", error);
    }
  };

  return (
    <AuthGuard>
      <div
        style={{
          minHeight: "100vh",
          backgroundColor: "#C3E6E3",
          display: "flex",
          flexDirection: "column",
        }}
      >
        <div
          style={{
            backgroundColor: "#A52A2A",
            padding: "10px",
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            width: "100%",
          }}
        >
          <div style={{ color: "white", fontSize: "24px", fontWeight: "bold" }}>
            Next App
          </div>
          <button
            style={{
              backgroundColor: "transparent",
              border: "none",
              color: "white",
              cursor: "pointer",
            }}
            onClick={handleSignOut}
          >
            Sign Out
          </button>
        </div>
        <div
          style={{
            flexGrow: 1,
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <h1>Welcome to the dashboard</h1>
        </div>
      </div>
    </AuthGuard>
  );
};

export default Dashboard;
