import Link from "next/link";
import Head from "next/head";

export default async function Home() {
  return (
    <div
      style={{
        backgroundColor: "#c4e0dd",
        minHeight: "100vh",
        padding: "20px",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <Head>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, viewport-fit=cover"
        />
      </Head>
      <nav
        style={{
          backgroundColor: "#c4e0dd",
          width: "100%",
          padding: "20px",
          marginBottom: "20px",
          border: "0.01px solid gray",
        }}
      >
        <h2 style={{ color: "black", margin: "0" }}>Next App</h2>
      </nav>
      <h1 style={{ textAlign: "center" }}>Welcome to my app</h1>
      <Link href="/signin">
        <button
          style={{
            backgroundColor: "brown",
            color: "white",
            padding: "10px 20px",
            borderRadius: "5px",
            border: "none",
            cursor: "pointer",
          }}
        >
          Sign in
        </button>
      </Link>
    </div>
  );
}
