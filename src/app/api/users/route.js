import { PrismaClient } from "@prisma/client";
import { NextResponse } from "next/server";

const prisma = new PrismaClient();

export async function GET() {
  try {
    const users = await prisma.user.findMany();
    return NextResponse.json(users); // Return the response
  } catch (error) {
    console.error("Error fetching users:", error);
    return NextResponse.json({ status: 500, message: "Something went wrong" }); // Return the error response
  }
}

// export async function createUser(req, res) {
//   if (req.method === "POST") {
//     const { firstName, lastName, email, password } = req.body;
//     try {
//       const newUser = await prisma.user.create({
//         data: {
//           firstName,
//           lastName,
//           email,
//           password, // Note: You should handle password hashing and other validations here
//         },
//       });
//       res.status(201).json(newUser);
//     } catch (error) {
//       console.error("Error creating user:", error);
//       res.status(500).json({ error: "Internal Server Error" });
//     }
//   } else {
//     res.setHeader("Allow", ["POST"]);
//     res.status(405).json({ error: `Method ${req.method} Not Allowed` });
//   }
// }
