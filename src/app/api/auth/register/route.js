import { NextResponse } from "next/server";
import * as bcrypt from "bcryptjs";
import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

export async function POST(request) {
  const body = await request.json();
  const { firstName, lastName, email, password } = body;

  // Check if there are any existing users in the database
  const existingUsers = await prisma.user.findMany();
  const isAdmin = existingUsers.length === 0; // First user becomes admin

  const userExists = await prisma.user.findUnique({ where: { email } });
  if (userExists) {
    return NextResponse.json({ status: 409, message: "User already exists" });
  }

  const hashedPassword = await bcrypt.hash(password, 12);
  const createdUSer = await prisma.user.create({
    data: {
      email,
      firstname: firstName,
      lastname: lastName,
      password: hashedPassword,
      isAdmin: isAdmin, // Assign isAdmin flag based on whether user is the first user
    },
  });
  return NextResponse.json(createdUSer);

  // if (isAdmin) {
  //   // Redirect to admin page if the first user is registered as admin
  //   return NextResponse.redirect("/admin");
  // } else {
  //   // Redirect to dashboard for regular users
  //   return NextResponse.redirect("/dashboard");
  // }
}
