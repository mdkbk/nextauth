import NextAuth from "next-auth";
import authOptions from "../[...nextauth]/[...nextauth].js";

export const { GET, POST } = NextAuth(authOptions);
