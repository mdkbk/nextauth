// import dotenv from "dotenv";
import { signOut } from "next-auth";
import NextAuth from "next-auth";
import { PrismaAdapter } from "@next-auth/prisma-adapter";
import GoogleProvider from "next-auth/providers/google";
import CredentialsProvider from "next-auth/providers/credentials";
import bcrypt from "bcryptjs";
import prisma from "@prisma/client";

// dotenv.config();

const authOptions = {
  adapter: PrismaAdapter(prisma),
  providers: [
    // Auth With Google
    // GoogleProvider({
    //   clientId:
    //     "950016166728-hubmidf25sda4rjig4f38p3vlm5i35vk.apps.googleusercontent.com",
    //   clientSecret: "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",
    // }),
    // GoogleProvider({
    //   clientId: process.env.GOOGLE_CLIENT_ID,
    //   clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    // }),
    // Auth With Email & Password
    CredentialsProvider({
      name: "credentials",
      credentials: {
        email: { label: "email", type: "text" },
        password: { label: "password", type: "password" },
      },
      async authorize(credentials) {
        if (!credentials?.email || !credentials?.password) {
          throw new Error("Empty Fields");
        }

        const user = await prisma.user.findUnique({
          where: {
            email: credentials.email,
          },
        });

        if (!user || !user?.hashedPassword) {
          throw new Error("Invalid credentials");
        }

        const isCorrectPassword = await bcrypt.compare(
          credentials.password,
          user.hashedPassword
        );

        if (!isCorrectPassword) {
          throw new Error("Invalid credentials");
        }
        return user;
      },
    }),
  ],
  pages: {
    signIn: "/",
  },
  secret: process.env.SECRET,

  session: {
    strategy: "jwt",
  },
  debug: process.env.NODE_ENV === "development",
};

// const handler = NextAuth(authOptions);
// export { handler as GET, handler as POST };
module.exports = authOptions;
