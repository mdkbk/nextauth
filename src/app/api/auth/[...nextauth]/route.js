import NextAuth from "next-auth";
import authOptions from "./[...nextauth]";

export const { GET, POST } = NextAuth(authOptions);
