import { NextResponse } from "next/server";
import * as bcrypt from "bcryptjs";
import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();

export async function POST(request) {
  const body = await request.json();
  const { email, password } = body;

  // Find the user in the database
  const user = await prisma.user.findUnique({ where: { email } });

  // If user doesn't exist, return error
  if (!user) {
    return NextResponse.json({ status: 404, message: "User not found" });
  }
  // Check if the provided password matches the stored hashed password
  const passwordMatch = await bcrypt.compare(password, user.password);

  if (!passwordMatch) {
    // If passwords don't match, return error
    return NextResponse.json({
      status: 401,
      message: "Invalid email or password",
    });
  }

  // If email and password are correct, return success
  return NextResponse.json({
    status: 200,
    message: "Signed in successfully",
    user,
  });
}
