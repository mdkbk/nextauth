"use client";
import React from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import toast from "react-hot-toast";
import { useRouter } from "next/navigation";
import Link from "next/link";
import "../styles/register.css";
import { FcGoogle } from "react-icons/fc";
import axios from "axios";

const SignupSchema = Yup.object().shape({
  firstName: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
  lastName: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
  email: Yup.string().email("Invalid email").required("Required"),
  password: Yup.string().required("Required"),
  password2: Yup.string()
    .oneOf([Yup.ref("password"), null], "Passwords must match")
    .required("Required"),
});

const register = () => {
  const router = useRouter();
  const handleRegister = async (values) => {
    try {
      await axios.post("/api/auth/register", values).then((response) => {
        console.log(response);
        if (response.data.status === 409) {
          toast.error("User already exists");
          return;
        }
        toast.success("User Created");
        router.push("/signin");
      });
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className="authForm">
      <h1>Signup</h1>

      <Formik
        initialValues={{
          firstName: "",
          lastName: "",
          email: "",
          password: "",
          password2: "",
        }}
        validationSchema={SignupSchema}
        onSubmit={(values) => {
          handleRegister(values);
          // same shape as initial values
          console.log(values);
        }}
      >
        {({ errors, touched }) => (
          <Form className="formField">
            <Field name="firstName" placeholder="firstname" />
            {errors.firstName && touched.firstName ? (
              <div>{errors.firstName}</div>
            ) : null}
            <Field name="lastName" placeholder="lastname" />
            {errors.lastName && touched.lastName ? (
              <div>{errors.lastName}</div>
            ) : null}
            <Field name="email" type="email" placeholder="email" />
            {errors.email && touched.email ? <div>{errors.email}</div> : null}
            <Field name="password" type="password" placeholder="password" />
            {errors.password && touched.password ? (
              <div>{errors.password}</div>
            ) : null}
            <Field
              name="password2"
              type="password"
              placeholder="re-type password"
            />
            {errors.password2 && touched.password2 ? (
              <div>{errors.password2}</div>
            ) : null}
            <br />
            Already have an account? <Link href="/signin">Signin</Link>
            <br />
            <button type="submit">Submit</button>
            <br />
            {/* <div className="google-signin-button">
              or Sign in with Google
              <a
                href="/api/auth/signin/google"
                className="google-signin-button"
              >
                <FcGoogle />
              </a>
            </div> */}
          </Form>
        )}
      </Formik>
      <div></div>
    </div>
  );
};

export default register;
