"use client";

import { useRouter } from "next/navigation";
import AdminGuard from "../components/adminguard";
import { useEffect, useState } from "react";
import axios from "axios";

const AdminDashboard = () => {
  const [users, setUsers] = useState([]);
  const router = useRouter();

  useEffect(() => {
    const fetchUsers = async () => {
      try {
        const response = await axios.get("/api/users"); // Fetch users from backend
        setUsers(response.data);
      } catch (error) {
        console.error("Error fetching users:", error);
      }
    };

    fetchUsers();
  }, []);

  const handleSignOut = async () => {
    try {
      // Implement your sign-out logic here
      localStorage.removeItem("accessToken");
      localStorage.removeItem("isAdmin");
      console.log("User signed out");
      router.push("/signin"); // Redirect to sign-in page after sign-out
    } catch (error) {
      console.error("Error signing out:", error);
    }
  };

  return (
    <AdminGuard>
      <div
        style={{
          backgroundColor: "#C3E6E3",
          textAlign: "center",
          minHeight: "100vh",
        }}
      >
        <div
          style={{
            backgroundColor: "#A52A2A",
            padding: "10px",
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          <div style={{ color: "white", fontSize: "24px", fontWeight: "bold" }}>
            Next App
          </div>
          <button
            style={{
              backgroundColor: "transparent",
              border: "none",
              color: "white",
              cursor: "pointer",
            }}
            onClick={handleSignOut}
          >
            Sign Out
          </button>
        </div>
        <h1>Welcome to the Admin Dashboard</h1>
        <h2 style={{ textDecoration: "underline" }}>User List</h2>
        <table style={{ borderCollapse: "collapse", width: "100%" }}>
          <thead>
            <tr>
              <th style={{ border: "1px solid black", padding: "8px" }}>
                First Name
              </th>
              <th style={{ border: "1px solid black", padding: "8px" }}>
                Last Name
              </th>
              <th style={{ border: "1px solid black", padding: "8px" }}>
                Email
              </th>
              <th style={{ border: "1px solid black", padding: "8px" }}>
                Role
              </th>
            </tr>
          </thead>
          <tbody>
            {users.map((user) => (
              <tr key={user.id}>
                <td style={{ border: "1px solid black", padding: "8px" }}>
                  {user.firstname}
                </td>
                <td style={{ border: "1px solid black", padding: "8px" }}>
                  {user.lastname}
                </td>
                <td style={{ border: "1px solid black", padding: "8px" }}>
                  {user.email}
                </td>
                <td style={{ border: "1px solid black", padding: "8px" }}>
                  {user.isAdmin ? "Admin" : "User"}
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </AdminGuard>
  );
};

export default AdminDashboard;
